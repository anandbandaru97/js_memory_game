const container = document.getElementById('container');
const button = document.getElementById('start');
const time = document.getElementById('time');
const moves = document.getElementById('moves');

document.addEventListener('DOMContentLoaded', function () {
    const cardContainer = document.createElement('div');
    cardContainer.classList.add('card-container');
    const emojis = ["🙂", "😍", "🥑", "🐱", "🌼", "🎉", "🍕", "🐧", "🙂", "😍", "🥑", "🐱", "🌼", "🎉", "🍕", "🐧"];
    const shuffleEmojis = emojis.sort(() => Math.random() - .5);

    let movesCount = 0;
    let timer;
    let seconds = 0;

    button.addEventListener('click', function () {
        button.classList.add('change');
        timer = setInterval(function () {
            seconds++;
            time.textContent = `Time: ${seconds} secs`;
        }, 1000);
    });

    let totalFlippedPairs = 0;
    for (let i = 0; i < 16; i++) {
        const card = document.createElement('div');
        card.classList.add('card');
        card.innerHTML = shuffleEmojis[i];
        cardContainer.append(card);

        card.onclick = function () {
            if (button.classList.contains('change')) {
                card.classList.add('cardFlip');

                let cardFlip = document.querySelectorAll('.cardFlip');
                let cardFlip1 = document.querySelectorAll('.cardFlip')[0];
                let cardFlip2 = document.querySelectorAll('.cardFlip')[1];
                movesCount++;
                moves.textContent = `Moves: ${movesCount}`;

                setTimeout(function () {

                    if (cardFlip.length > 1) {

                        if (cardFlip1.innerHTML == cardFlip2.innerHTML) {
                            cardFlip1.classList.add('cardMatch');
                            cardFlip2.classList.add('cardMatch');

                            cardFlip2.classList.remove('cardFlip');
                            cardFlip1.classList.remove('cardFlip');

                            totalFlippedPairs++;
                        }

                        if (totalFlippedPairs == emojis.length / 2) {
                            clearInterval(timer);
                            alert('Congratulations!');
                        } else {
                            cardFlip2.classList.remove('cardFlip');
                            cardFlip1.classList.remove('cardFlip');
                        }
                    }
                }, 500);
            }
        }
    }
    container.append(cardContainer);
});